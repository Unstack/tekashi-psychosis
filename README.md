## Tekashi Psychosis
A bunch of Conky scripts inspired by Tekashi.

![Tekashi Psychosis preview image](https://i.redditmedia.com/8ZDfELapSHHRib5cA5U3Zr8v96P3d7RMlabaz2OuGJ0.png?fit=crop&crop=faces%2Centropy&arh=2&w=640&s=73de0757aa5072ab0fa61282a1a46dd3)

### Requirements
* calcurse
* audtool
* Internet connection

### Installation
```bash
git clone git@gitlab.com:Unstack/tekashi-psychosis.git
cd tekashi-psychosis
installer.sh
```

*Manual installation:*  
Extract everything to your Conky folder. Move scripts over to ~/scripts. Enjoy.

Logo is pointing towards an image in your home/pictures directory. Adjust accordingly.

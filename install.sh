#!/bin/sh
echo -e "\e[92m696969696969696969696969696969696969\e[39m"
echo "Tekash-psychosis install script"
echo -e "\e[92m696969696969696969696969696969696969\e[39m"

echo "Warning: If you don't have a 'scripts' directory in your home, it will be created."
echo "A directory for artist badges will also be created in '$(xdg-user-dir PICTURES)/artists' (png only)."
echo "Hit enter key to continue or anything else to bail:"
read bail

if [ ! -z $bail ]; then
	echo "Okay; Bailing.."
	exit
fi

if [ ! -d "/home/$USER/scripts" ]; then
	echo "Creating directory /home/$USER/scripts"
	mkdir "/home/$USER/scripts"
fi

if [ ! -d "$(xdg-user-dir PICTURES)/artists" ]; then
	echo "Creating directory $(xdg-user-dir PICTURES)/artists"
	mkdir "/home/$USER/scripts"
fi

if [ ! -d "/home/$USER/.conky" ]; then
	echo "Warning: Conky directory was not found."
	echo "Making a .conky directory for sanity."
	mkdir "/home/$USER/.conky"
fi

if [ ! -d "/home/$USER/.conky/fonts" ]; then
	echo "Creating directory /home/$USER/scripts/fonts"
	mkdir "/home/$USER/.conky/fonts"
fi

if [ ! -d "/home/$USER/.conky/tekashi-psychosis" ]; then
	echo "Making directory /home/$USER/.conky/tekashi-psychosis"
	mkdir "/home/$USER/.conky/tekashi-psychosis"
fi

echo "Installing scripts to /home/$USER/scripts"

cp "conky-load-colors.py" 	"/home/$USER/scripts"

echo "Installing fonts to /home/.conky/fonts"
cp "fonts/Ubuntu.ttf" 	"/home/$USER/.conky/fonts"

echo "Installing Conky files to /home/$USER/.conky/tekashi-psychosis"

cp "logo"               "/home/$USER/.conky/tekashi-psychosis"
cp "music"              "/home/$USER/.conky/tekashi-psychosis"
cp "agenda"             "/home/$USER/.conky/tekashi-psychosis"
cp "sidebar"            "/home/$USER/.conky/tekashi-psychosis"

cp "music.lua"          "/home/$USER/.conky/tekashi-psychosis"
cp "agenda.lua"         "/home/$USER/.conky/tekashi-psychosis"
cp "sidebar.lua"        "/home/$USER/.conky/tekashi-psychosis"

cp "tekashi.png"        "$(xdg-user-dir PICTURES)/artists"

echo "Copying tekashi.sh to ~/.conky"

cp "tekashi.sh"  "/home/$USER/.conky"

echo -e "\e[92m696969696969696969696969696969696969\e[39m"
echo "Done. Run 'sh ~/.conky/tekashi.sh' to start."
echo -e "\e[92m696969696969696969696969696969696969\e[39m"

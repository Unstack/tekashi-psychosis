require "cairo"

function conky_audtool_progress()
  local current = tonumber(conky_parse('${exec audtool current-song-output-length-frames}'))
  local total = tonumber(conky_parse('${exec audtool current-song-length-frames}'))

  if current ~= null and
  total ~= null then
    return current / total * 100
  else
    return 0
  end
end

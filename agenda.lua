function conky_artist_image(path,width,height,size)
  local artist = conky_parse('${exec audtool --current-song-tuple-data artist}')

  if artist == "" then
    artist = "tekashi"
  end

  return "${image "..path.."/"..artist..".png -p "..width..","..height.." -s "..size.."}"
end
